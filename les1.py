import telebot
import json

with open('token.json', 'r') as f:
	token = json.load(f)['TOKEN']
	print(token)

bot = telebot.TeleBot(token)

@bot.message_handler(content_types=["text"])
def repeat_all_messages(message):
    bot.send_message(message.chat.id, message.text)

if __name__ == '__main__':
    bot.polling(none_stop=True)